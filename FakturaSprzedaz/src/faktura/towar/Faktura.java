package faktura.towar;

import java.util.Calendar;
import java.util.Date;

public class Faktura {
	
	private Towar towar;
	private Calendar dataSprzedazy;
	private int terminZaplatyDni;
	private Calendar terminZaplatyData;
	private String numerFaktury;
	
	private double cenaCalkowitaNetto;
	private double cenaCalkowitaBrutto;
	private double procent;
	private double podatekVat;
	
	public Faktura()
	{
		dataSprzedazy = Calendar.getInstance();
		terminZaplatyData = Calendar.getInstance();
	}

	public void setDataSprzedazy(Calendar dataSprzedazy){
		this.dataSprzedazy = dataSprzedazy;
	}
	
	public Calendar getDataSprzedazy(){
		return this.dataSprzedazy;
	}
	
	public void setTerminZaplatyDni(int terminZaplatyDni){
		this.terminZaplatyDni = terminZaplatyDni;
		terminZaplatyData.add(Calendar.DAY_OF_MONTH, terminZaplatyDni);
	}
	
	public int getTerminZaplatyDni(){
		return this.terminZaplatyDni;
	}
	
	public void setTerminZaplatyData(Calendar terminZaplatyData){
		this.terminZaplatyData = terminZaplatyData;
	}
	
	public Calendar getTerminZaplatyData(){
		return this.terminZaplatyData;
	}
	
	public void setNumerFaktury(String numerFaktury){
		this.numerFaktury = numerFaktury;
	}
	
	public String getNumerFaktury(){
		return this.numerFaktury;
	}
	
	public double getCenaCalkowitaNetto(){
		this.cenaCalkowitaNetto = towar.getCenaNetto()*towar.getIlosc();
		return this.cenaCalkowitaNetto;
	}
	
	public double getCenaCalkowitaBrutto(){
		this.cenaCalkowitaBrutto = towar.getCenaNetto()*towar.getIlosc()*(1+procent);
		return this.cenaCalkowitaBrutto;
	}
	
	public void setProcent(double procent){
		this.procent = procent;
	}
	
	public double getProcent(){
		return this.procent;
	}
	
	public void setPodatekVat(double podatekVat){
		this.podatekVat = podatekVat;
	}
	
	public double getPodatekVat(){
		this.podatekVat = towar.getCenaBrutto() - towar.getCenaNetto();
		return this.podatekVat;
	}
}

//Faktura(String nazwaTowaru, String jednostka, double cenaNetto, double cenaBrutto, double ilosc, double cenaCalkowitaNetto, double cenaCalkowitaBrutto, double procent, Calendar dataSprzedazy, int terminZaplatyDni, Calendar terminZaplatyData, double cenaCalkowitaNetto2, double cenaCalkowitaBrutto2, String numerFaktury)
//{
//	this.nazwaTowaru = nazwaTowaru;
//	this.jednostka = jednostka; 
//	this.cenaNetto = cenaNetto; 
//	this.cenaBrutto = cenaBrutto;
//	this.ilosc = ilosc;
//	this.cenaCalkowitaNetto = cenaCalkowitaNetto;
//	this.cenaCalkowitaBrutto = cenaCalkowitaBrutto;
//	this.procent = 23;
//	
//	this.dataSprzedazy = dataSprzedazy;
//	this.terminZaplatyDni = terminZaplatyDni;
//	this.terminZaplatyData = terminZaplatyData;
//	this.cenaCalkowitaNetto2 = cenaCalkowitaNetto2;
//	this.cenaCalkowitaBrutto2 = cenaCalkowitaBrutto2;
//	this.numerFaktury = numerFaktury;
//}
//
//Faktura(Faktura faktura)
//{
//	this.nazwaTowaru = faktura.nazwaTowaru;
//	this.jednostka = faktura.jednostka; 
//	this.cenaNetto = faktura.cenaNetto; 
//	this.cenaBrutto = faktura.cenaBrutto;
//	this.ilosc = faktura.ilosc;
//	this.cenaCalkowitaNetto = faktura.cenaCalkowitaNetto;
//	this.cenaCalkowitaBrutto = faktura.cenaCalkowitaBrutto;
//	this.procent = faktura.procent;
//	
//	this.dataSprzedazy = faktura.dataSprzedazy;
//	this.terminZaplatyDni = faktura.terminZaplatyDni;
//	this.terminZaplatyData = faktura.terminZaplatyData;
//	this.cenaCalkowitaNetto2 = faktura.cenaCalkowitaNetto2;
//	this.cenaCalkowitaBrutto2 = faktura.cenaCalkowitaBrutto2;
//	this.numerFaktury = faktura.numerFaktury;
//}
