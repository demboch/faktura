package faktura.towar;
import java.util.ArrayList;

public class Towar {

	private Faktura faktura;
	private String nazwaTowaru; // nazwa towaru 
	private String jednostka; // jednostka w ktorej sprzedawany jest dany towar (tony, sztuki, metry itd.)
	private double cenaNetto; // cena jednostkowa netto
	private double cenaBrutto; // cena jednostkowa brutto
	private int ilosc;
	
	public static void spisTowaru()
	{
		ArrayList<String> spis = new ArrayList<String>(); // kolekcje 

		spis.add("");
		spis.add("     **************************************** ");
		spis.add("     *            Wybierz Pordukt           * ");
		spis.add("     **************************************** ");
		spis.add("     *        1. Audi                       * ");
		spis.add("     *        2. BMW                        * ");
		spis.add("     *        0. Koniec                     * ");
		spis.add("     ****************************************\n ");

		for(String s : spis) { System.out.println(s); }
    }
	
	public void setNazwaTowaru(String nazwaTowaru){
		this.nazwaTowaru = nazwaTowaru;
	}
	
	public String getNazwaTowaru(){
		return this.nazwaTowaru;
	}
	
	public void setJednostka(String jednostka){
		this.jednostka = jednostka;
	}
	
	public String getJednostka(){
		return this.jednostka;
	}
	
	public double getCenaNetto(){
		return this.cenaNetto;
	}
	
	public void setCenaNetto(double cenaNetto){
		this.cenaNetto = cenaNetto;
	}
	
	public double getCenaBrutto(){
		this.cenaBrutto = cenaNetto*(1+faktura.getProcent());
		return this.cenaBrutto;
	}
	
	public void setIlosc(int ilosc){
		this.ilosc = ilosc;
	}
	
	public int getIlosc(){
		return this.ilosc;
	}	
}
