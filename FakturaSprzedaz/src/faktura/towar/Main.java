package faktura.towar;

import java.util.Calendar;
import java.util.Scanner;

public class Main {

	private static int produkt;
	
	public static void main(String[] args){

		Scanner in = new Scanner(System.in);
		Towar towar = new Towar();
		Towar towar2 = new Towar();
		Faktura faktura = new Faktura();
		
		boolean koniec = false;
		
		do{
			Towar.spisTowaru();
			produkt = in.nextInt(); // przypisywanie zmiennej statycznej produkt warot�ci wczytanej z klawiatury
			  
            switch (produkt) {
            case 1:
            	towar.setNazwaTowaru("Samochod osobowy Audi ");
        		towar.setJednostka("sztuki");
        		towar.setCenaNetto(200000);
        		towar.setIlosc(5);
        		
        		faktura.setNumerFaktury("12/9/2014");
        		faktura.setTerminZaplatyDni(14);
        		faktura.setProcent(0.23);
        		
        		System.out.print("Nazwa towaru: "+ towar.getNazwaTowaru() +"\nJednostka: "+ towar.getJednostka() +"\nCena Netto: "
        						+ towar.getCenaNetto() +"\nCena Brutto: "+ towar.getCenaBrutto() +"\nIlosc sprzedanych sztuk: "
        						+ towar.getIlosc());
        		
        		System.out.print("\nData wystawienia: "+ faktura.getDataSprzedazy().getTime() + faktura.getTerminZaplatyData().getTime() 
        						+"\nCalkowita cena netto: "+ faktura.getCenaCalkowitaNetto() +"\nCalkowita cena brutto: "
        						+ faktura.getCenaCalkowitaBrutto() +"\nPodatek VAT: "+ faktura.getProcent() +"\n");
                  break;
//            case 2:
//            	towar2.setNazwaTowaru("Samochod osobowy BMW");
//        		towar2.setJednostka("sztuki");
//        		towar2.setCenaNetto(250000);
//        		towar2.setIlosc(2);
//        		towar2.setProcent(0.23);
//        		
//        		System.out.print("Nazwa towaru: "+ towar2.getNazwaTowaru() +"\nJednostka: "+ towar2.getJednostka() +"\nIlosc przedanych sztuk: "
//						+ towar2.getIlosc() +"\nCena Netto: "+ towar2.getCenaNetto() +"\nCena Brutto: "
//						+ towar2.getCenaBrutto() +"\nCalkowita cena netto: "+ towar2.getCenaCalkowitaNetto() +"\nCalkowita cena brutto: "
//						+ towar2.getCenaCalkowitaBrutto() +"\nPodatek VAT: "+ towar2.getProcent() +"\n");
//                  break;
			case 0:
	            koniec = true;
	            break;
            }
		}
		while(!koniec);
	}
}
