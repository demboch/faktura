package cart;

public class Product {

	String name;
	double prize;
		
	public Product(String name, double prize)
	{
		this.name = name;
		this.prize = prize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrize() {
		return prize;
	}

	public void setPrize(double prize) {
		this.prize = prize;
	}


}
