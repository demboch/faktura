package Discounts;

import java.util.List;

import cart.CartPosition;

public interface Discount {
		void setCartPosition(CartPosition x);
		void discount();
		void ChangeList(List<CartPosition> x);

}
